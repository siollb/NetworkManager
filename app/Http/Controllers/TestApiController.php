<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BindApiService;

class TestApiController extends Controller
{
    public function display()
{
    return view('dns.test_api');
}

public function getAllRecords()
{
// Instanciate BindRestApi Service
$bindRestApi = new BindApiService();
// Use the getAllZoneRecords method to get all zone records
$records = $bindRestApi->getAllZoneRecords();
return view('dns.records', ['records' => $records]);
}
}

