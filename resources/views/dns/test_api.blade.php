

@extends('layouts.app', ['activePage' => 'api', 'titlePage' => __('Test des actions')])


@section('content')
<div class="content">
<div class="col ml-5" style="width: 100%;">
<div style="width: 50%; height: 100px; float: left;">
<a href="{{ route('api.records')}}" class="btn btn-success">Voir tous les enregistrements</a>
</div>
</div>
<div class="container-fluid">
@if(isset($records))
<div class="row">
<div class="col-12">
{!! print_r($records) !!}
@endif
</div>
</div>
@endsection
