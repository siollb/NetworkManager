@extends('layouts.app', ['activePage' => 'dns', 'titlePage' => __('Editer DNS')])

@section('content')

@foreach ($records as $record)
<p>Name : {{ $record->name }}</p>
<p>Type {{ $record->rrtype }} : {{ $record->response }} : {{ $record->ttl }}</p>
@endforeach

@endsection